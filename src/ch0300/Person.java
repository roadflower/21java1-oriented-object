package ch0300;

public   class Person {
	 String name;
	
	private int age;
	private char gender;
	//类成员变量
	 static int legNum;
	//类成员方法
	public static int getLegNum() {
		return legNum;
	}
	//类方法
	public static void setLegNum(int num) {
	//	age=9;
		legNum=num;
	}
    //实例方法
	public String getName() {
		legNum=5;
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public char getGender() {
		return gender;
	}
	
	public void setGender(char gender) {
		this.gender = gender;
	}
//多态	
	public void print() {
		System.out.println("执行Person类中的print()方法");
	}
	public String print(String a) {
		System.out.println("执行Person类中的print (a)方法"+a);
		return a;
	}
	//抽象
	public  void doSomething() {}  ;
}
