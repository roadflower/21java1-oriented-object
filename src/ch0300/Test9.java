package ch0300;

public class Test9 {

	public static void main(String[] args) {
		Teacher xiongTeacher=new Teacher();
		Student huangStudent=new Student();
		Student wuStudent=new Student();
		Student heStudent=new Student();
		Student liaoStudent=new Student();
		Supervisor liSupervisor=new Supervisor();
		liSupervisor.supervisor(xiongTeacher);
		liSupervisor.supervisor(huangStudent);
		liSupervisor.supervisor(wuStudent);
		liSupervisor.supervisor(heStudent);
		liSupervisor.supervisor(liaoStudent);
		
		Worker ccy=new Worker();
		liSupervisor.supervisor(ccy);
		
	}
}
