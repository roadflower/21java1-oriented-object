package ch0300;
//类变为抽象类
public abstract class Common {
	float A,B,C,length;
	public Common(float A,float B,float C,float length) {
		this.A=A;
		this.B=B;
		this.C=C;
		this.length=length;
	}
	

	//本来是一个空方法的，定义为一个抽象方法，不用实现;加一个修饰符abstract
	public abstract float computeSpeed();
	public float time() {
		return length/computeSpeed();
	}

}
