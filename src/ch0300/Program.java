package ch0300;

public class Program {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		
		/*
		 * Common car=new Car(5,6,7); //将父类向下转换为子类，向下转型 Car c=(Car)car;
		 * System.out.println(1000/car.computeSpeed()); Common plane=new Plane(5,6,7);
		 * 
		 * 
		 * if(plane instanceof Car) { Car car2=(Car)plane;
		 * 
		 * }else if(plane instanceof Plane){ Plane plane2 = (Plane)plane; }
		 * 
		 * 
		 * System.out.println(1000/plane.computeSpeed()); Common ship=new Ship(5,6,7);
		 * System.out.println(ship.computeSpeed());
		 * 
		 * System.out.println(200/ship.computeSpeed()+200/plane.computeSpeed()+200/car.
		 * computeSpeed());
		 */ 
		 
		Common[] common= {new Ship(10,10,10,200),new Plane(10,10,10,200),new Car(10,10,10,200)};
		float time=0;
		//指针遍历
		for(Common co:common) {
		     time+=co.time();
		     
		}
		System.out.println(time); 	
		final float pi=3.14f;
		
		Teacher xiongTeacher=new Teacher();
		
		Teacher daiTeacher=new Teacher();
		daiTeacher.setLegNum(8);
		System.out.println(daiTeacher.getLegNum());
		
		Student huangStudent=new Student();
		System.out.println(Teacher.getLegNum());

		Car benzCar=new Car(10,10,10,500);
		benzCar.setDriver(xiongTeacher);
		benzCar.go();
		
		
	}

}
