package ch0300;

public class Circle extends Shape{
	public int r;
	public Circle(int r) {
		this.r=r;
	}
	public int getR() {
		return r;
	}
	public void setR(int r) {
		this.r = r;
	}
	public float area() {
		float s=(float) (3.14*r*r);
		System.out.println("半径为"+r+"的圆形面积为："+s);
		return s;
	}
}
