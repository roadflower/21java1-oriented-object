package ch0300;

import org.junit.Test;

public class Student extends Person implements Driver{
	private String grade;
    public Student() {
    	
    }

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	
	  //子类的方法与父类的同名，覆写
	  
	  public void print() { 
		  super.print();
		  System.out.println(",目前我上 "+grade); }
	 
	  public void doSomething() {
		  System.out.println("学生认真上课");
	  }

	@Override
	public void getLicence() {
		// TODO 自动生成的方法存根
		System.out.println("我是学生，我有驾照");
	};
}
