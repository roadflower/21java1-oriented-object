package ch1100;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextArea;

//实现行为监听器接口
public class MyPicture implements ActionListener,MouseWheelListener{
	static JFrame myFrame;
	static JButton button;
	static JTextArea textArea;
	static JComboBox  comboBox;
//倒过来的第一象限
	public static void main(String[] args) {
		// TODO 自动生成的方法存根,frame是容器
           myFrame=new JFrame("this is my frame");
           //三行2列的网格布局
           myFrame.setLayout(new GridLayout(3,2));
           myFrame.setBounds(100, 100, 1000, 500);
           myFrame.setBackground(Color.green);
          
           textArea=new JTextArea("this is my textarea");
           textArea.setBounds(200, 200, 50, 10);
           textArea.setCaretColor(Color.red);
           myFrame.add(textArea,"1");
           //监听器Listener
           button=new JButton("click here");
           button.setBounds(400, 400, 20, 20);
           button.addActionListener(new MyPicture());
           myFrame.add(button,"2");
           
           comboBox=new JComboBox<>();
           List list=new ArrayList<>();
           list.add("java1");
           list.add("java2");
           list.add("java3");
           comboBox.setModel(new DefaultComboBoxModel<>(list.toArray()));
           myFrame.add(comboBox,"3");
           //让容器显现
           myFrame.setVisible(true);
	}
	//实现监听方法
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO 自动生成的方法存根
		if(e.getSource()==button)
			if(textArea.getText().equals("this is my textarea"))
			textArea.setText("you have clicked that button");
			else {
				textArea.setText("this is my textarea");
			}
	}
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		
		textArea.setText("you are up mouse wheel");
		// TODO 自动生成的方法存根
		
	}


}
