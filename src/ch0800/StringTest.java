package ch0800;

import java.util.Calendar;
import java.util.Scanner;

public class StringTest {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		String xinxiString="01ab210417\t张三\t67";
		String anoString=new String("01ab210417\t张三\t67");
		System.out.println(xinxiString.equals(anoString));
		
		String[] str=xinxiString.split("\t");
		System.out.println(str[0]+", "+str[1]+",成绩是 "+str[2]);
		
		
		Scanner input=new Scanner(System.in);
		System.out.println("请输入一个10位数字组成的学号：");
		String id=input.next();
		while(id.length()<10) {
			System.out.println("学号应该是10位的，输入有误，请重新输入!");
			id=input.next();
		}
		//取子串，4是索引begin,6是end
		String yearStr=id.substring(4,6);
		int year=Integer.parseInt(yearStr);
		//当前年份
		//Calendar日历
		
		int nowYear=Calendar.getInstance().get(Calendar.YEAR);
		//当前月份
		int nowMonth=Calendar.getInstance().get(Calendar.MONTH)+1;
		nowYear=nowYear%100;
		//上半年，年级就是当前年-入学年
		int gradeYear=nowYear-year;
		//下半年，年级+1
		if(nowMonth>=9) {
			gradeYear=gradeYear+1;
		}
		String grade="";
		if(gradeYear<=0) {
			grade="还没就读!";
		}else if(gradeYear==1) {
			grade="大一";
		}else if(gradeYear==2) {
			grade="大二";
		}else if(gradeYear==3) {
			grade="大三";
		}else if(gradeYear==4) {
			grade="大四";
		}else {
			grade="毕业"+(gradeYear-4)+"年！";
		}
		System.out.println("该学生现在是"+grade);
		

	}

}
