import ch0700.Triangle;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

interface IShape{
	double area();
}
//alt+enter
class Circle implements IShape{
	private int r;
//alt+insert
	public Circle(int r) {
		this.r = r;
	}
	@Override
	public double area() {
		return Math.PI*r*r;
	}
}
class Square implements  IShape{
	private float x,y;

	public Square(float x, float y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public double area() {
		return x*y;
	}
}
class MyTriangle implements  IShape{
	private float di,high;

	public MyTriangle(float di, float high) {
		this.di = di;
		this.high = high;
	}

	@Override
	public double area() {
		return di*high/2;
	}
}
class Point{
	private float x,y;

	public Point(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	public String toString(){
		return "这个点的坐标x是"+x+",y是"+y;
	}
	public double distance(Point p){
		return Math.sqrt(Math.pow((x-p.getX()),2)+Math.pow((y-p.getY()),2));
	}
}
class Food{
	private int id;
	private String name;
	private float price;
	private int num;

	public Food(int id, String name, float price, int num) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.num = num;
	}
	public String toString(){
		return id+"  "+name+"  "+price+"  "+num;
	}
}
public class TestMain{
	List<Food> foodList=new ArrayList<>();
	private static int m;

	public static void main(String args[]){
		float f=0.23f;
		long l=0Xfff;
		long a[]=new long[10];
		System.out.println(a[6]);

		/*IShape[] shapes={new Circle(1),new Square(2,4),new MyTriangle(6,5)};
		double area=0;
		for (IShape shape:shapes)
			area+=shape.area();
		System.out.println(area);
		Point p1=new Point(1,2);
		Point p2=new Point(3,4);
		System.out.println(p1.distance(p2));*/
		try {
			BufferedReader bufferedReader=new BufferedReader(new FileReader("C:\\Users\\intel\\Downloads\\order.txt"));
			PrintWriter printWriter=new PrintWriter(new FileWriter("C:\\Users\\intel\\Downloads\\orderDetail.txt"));
			String line;
			String[] foodStr;
			System.out.println("         订单列表");
			printWriter.println("        订单列表");
			int i=0;//菜品的数量
			int totalAmount=0;

			while ((line=bufferedReader.readLine())!=null){
				foodStr=line.split(",");
				Food food=new Food(Integer.parseInt(foodStr[0]),foodStr[1],Float.parseFloat(foodStr[2]),Integer.parseInt(foodStr[3]));
				//foodList.add(food);
				System.out.println(food);
				printWriter.println(food);
				i++;
				totalAmount+=Float.parseFloat(foodStr[2])*Integer.parseInt(foodStr[3]);
			}
			System.out.println("该顾客订了"+i+"种菜品，总金额为:"+totalAmount);
			printWriter.println("该顾客订了"+i+"种菜品，总金额为:"+totalAmount);
			printWriter.flush();
			printWriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
