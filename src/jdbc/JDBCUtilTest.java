package jdbc;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//用于单元测试
public class JDBCUtilTest {
    private String sql="select * from employee where departmentid=?";
    private List<Object> params=new ArrayList<>();
    //insert,update,delete语句执行影响的记录条数
    private int result=2;
    @Before
    public void setUp(){
       /* params.add(1);
        params.add("陈守鑫");
        params.add("chen");
        params.add("13880098888");
        params.add("julia@qq.com");
        params.add("1");
        params.add("1");
        params.add("123456");
        params.add(1);*/
        params.add(4);

    }
    @Test
    public void test(){
       // Assert.assertEquals(result,JDBCUtil.execStatement(sql,params));
        try {
            //判断结果集的记录个数跟你的预期是否一致

            ResultSet rs=JDBCUtil.execQuery(sql,params);
            //将结果集的指针移动到最后一条记录
            rs.last();
            Assert.assertEquals(result,rs.getRow());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @After
    public void after(){
        JDBCUtil.closeAll();
    }

}
