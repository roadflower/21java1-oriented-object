package jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDao {
    public static void main(String args[]){

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/meeting?characterEncoding=utf-8&serverTimezone=UTC","root","");
            String sql="select * from employee where ?";
            PreparedStatement preparedStatement=connection.prepareStatement(sql);
            ResultSet resultSet=preparedStatement.executeQuery();
            List<Employee>  employeeList=new ArrayList<Employee>();
            //循环遍历结果集
            while (resultSet.next()){
                Employee employee=new Employee();
                //将取到的记录员工号赋值给对象
                employee.setEmployeeId(resultSet.getInt("employeeid"));
                employee.setEmployeeName(resultSet.getString(2));
                employee.setUsername(resultSet.getString(3));
                employee.setPassword(resultSet.getString("password"));
                employee.setPhone(resultSet.getString("phone"));
                employee.setEmail(resultSet.getString("email"));
                employee.setStatus(resultSet.getString("status"));
                employee.setDepartmentId(resultSet.getInt("departmentid"));
                employee.setRole(resultSet.getString("role"));
                //使用List存储员工对象
                employeeList.add(employee);
                //打印员工信息
               // System.out.println(employee);
            }
            //json
            System.out.println(employeeList);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
