package ch0600;

public class Printer {
	private int num;
	//类 类型，自己设计的类或者接口
	private Ink ink;
	private Paper paper;
	
	
	public Ink getInk() {
		return ink;
	}
	public void setInk(Ink ink) {
		this.ink = ink;
	}

	public Paper getPaper() {
		return paper;
	}
	public void setPaper(Paper paper) {
		this.paper = paper;
	}
	public void print() {
		System.out.println("我用的是"+paper.type()+"纸，用"+ink.color()+"颜色打印");
	}

	public static void main(String args[]) {
		Paper a4=new A4Paper();
		Paper B5=new B5Paper();
		Ink  black=new BlackInk();
		Ink  color=new ColorInk();
		
		Printer printer=new Printer();
		printer.setPaper(B5);
		printer.setInk(color);
		printer.print();
		
		printer.setPaper(a4);
		printer.setInk(color);
		printer.print();
	}
}
