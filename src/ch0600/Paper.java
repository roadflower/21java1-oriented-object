package ch0600;

public interface Paper {
  String type();
}

class A4Paper implements Paper{
	@Override
	public String type() {
		// TODO 自动生成的方法存根
		return "A4";
	}
}
class B5Paper implements Paper{
	//alt+/,快捷键
	@Override
	public String type() {
		// TODO 自动生成的方法存根
		return "B5";
	}
}