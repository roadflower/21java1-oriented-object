package ch0600;

public abstract class Shape {
	abstract void computeArea();

}
class Cirle extends Shape{
	private float r;
	public Cirle(float r) {
		// TODO 自动生成的构造函数存根
		this.r=r;
	}
	public float getR() {
		return r;
	}

	public void setR(float r) {
		this.r = r;
	}

	@Override
	void computeArea() {
		// TODO 自动生成的方法存根
		System.out.println(3.14*r*r);
	}
}
class Square extends Shape{
	private float length,width;
	public Square(float length,float width) {
		this.length=length;
		this.width=width;
	}
	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	@Override
	void computeArea() {
		// TODO 自动生成的方法存根
		System.out.println(length*width);
	}
}
