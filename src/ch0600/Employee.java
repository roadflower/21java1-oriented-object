package ch0600;

public abstract class Employee {
	abstract void setSalary(int s);
	abstract void showSalary(int s);

}
class Sales extends Employee{
	private int salary;
	@Override
	void setSalary(int s) {
		// TODO 自动生成的方法存根
		this.salary=s;
	}
	@Override
	void showSalary(int s) {
		// TODO 自动生成的方法存根
		System.out.println("销售员的薪水是"+s);
	}
	
}
class Manager extends Employee{
	private int salary;
	@Override
	void setSalary(int s) {
		// TODO 自动生成的方法存根
		this.salary=s;
	}
	@Override
	void showSalary(int s) {
		// TODO 自动生成的方法存根
		System.out.println("经理的薪水是"+s);
	}
	
}