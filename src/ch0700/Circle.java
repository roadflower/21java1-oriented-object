package ch0700;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Circle {
	public float countArea(float r) throws Exception{
		if (r<0) {
			throw new Exception("半径不能为负数！");
		}
		return (float) (3.14*r*r);
	}
	public static void main (String[] args) {
		//Java7新特性，finally关闭或者释放（bReader）里的资源
		try(BufferedReader bReader=new BufferedReader(new InputStreamReader(System.in))) {
			
			Circle circle = new Circle();
			circle.countArea(-3);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			//bReader.close();
		}
	}
}
