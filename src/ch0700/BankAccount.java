package ch0700;

public class BankAccount {
	private String idString;
	//Integer,
	private double balance;
	
	public BankAccount(String idString, double balance) {

		this.idString = idString;
		this.balance = balance;
	}
	public String getIdString() {
		return idString;
	}
	public void setIdString(String idString) {
		this.idString = idString;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	//存钱
	public void store(double money) {
		this.balance+=money;
		System.out.println("存了"+money+"钱");
	}
	//取钱
	public void withdraw(double money) throws OurException {
		// 判断取出的金额是否大于余额，如果是报异常
		if (money > this.balance) {
			throw new OurException("余额不足");
		} else {
			this.balance -= money;
			System.out.println("取了"+money+"钱");
		}
	}
	//转账
	public void transfer(BankAccount toAccount,double money) throws OurException{
		if (money > this.balance) {
			throw new OurException("余额不足");
		} else {
			this.balance -= money;
			toAccount.balance+=money;
			System.out.println("转给"+toAccount.idString+money+"钱");
		}
	}
	//显示余额
    public void showBalance() {
    	System.out.println("还剩下"+balance+"钱");
    }
    
    public static void main(String args[]) {
    	BankAccount fromAccount=new BankAccount("s12",300f);
    	BankAccount toAccount=new BankAccount("s34",200f);
    	fromAccount.store(300);
    	try {
			fromAccount.transfer(toAccount, 700);
			
		} catch (OurException e) {
			// TODO 自动生成的 catch 块
			System.out.println(e.getMessage());
		}
		finally {
			fromAccount.showBalance();
		}
    }
}
