package ch0700;

public class Triangle {
	private int a,b,c;
	public Triangle(int a,int b,int c)  {
		if((a+b<c)||(a+c<b)||(b+c<a)) {
			throw new IllegalArgumentException("abc不能构成三角形");
		}
		else {
			this.a=a;
			this.b=b;
			this.c=c;
			System.out.println("三角形三条边长是 "+a+","+b+","+c);
		}
	}

	/*
	 * public String toString() { return "三角形三条边长是 "+a+","+b+","+c; }
	 */
	public static void main(String[] args) {
		// TODO 自动生成的方法存根

		try {
			Triangle triangle1=new Triangle(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
			Triangle triangle2=new Triangle(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
			System.out.println(triangle1==triangle2);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
	}

}
