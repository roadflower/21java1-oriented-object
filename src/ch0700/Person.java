package ch0700;

public class Person {
	private int age;

	public int getAge() {
		return age;
	}

	public void setAge(int age) throws OurException{
		if (age < 0 || age > 150) {
			throw new OurException("年龄不科学"); // 自定义异常的使用
		} else {
			this.age = age;
		}
	}
	public static void main(String args[]) {
		Person person=new Person();
		try {
			person.setAge(500);
		} catch (OurException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

}
