package net;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class ChatServer {
    public static void main(String args[])  {
        //创建服务器套接口serverSocket
        ServerSocket serverSocket= null;
        Socket socket = null;

        try {
            serverSocket = new ServerSocket(8888);
       
        //让服务器永远处于等待连接的状态
        while (true){
            //服务器接收客户端的连接请求，创建套接口socket
             socket=serverSocket.accept();
            DataInputStream dis=new DataInputStream(socket.getInputStream());
            String clientName=dis.readUTF();

            //启动发送消息线程
            new SendMessage(socket).start();
            //启动接收消息线程
            new ReceiveMessage(socket).start();
        }
        }
        catch (IOException e) {
            e.printStackTrace();
           
        }
        finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
