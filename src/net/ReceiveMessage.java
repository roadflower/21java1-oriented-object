package net;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
//接收消息
public class ReceiveMessage extends  Thread{
    //网络套接口
    private Socket socket;

    public ReceiveMessage(Socket socket){
        this.socket=socket;
    }
    //线程的运行方法
    public void run(){
        //永远执行
        while (true){
            try {
                //消息来源于套接口
                DataInputStream dis=new DataInputStream(socket.getInputStream());
                //在控制台上打印从套接口读到的字符串
                System.out.println(dis.readUTF());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



}
