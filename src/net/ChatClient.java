package net;

import javax.swing.plaf.synth.SynthEditorPaneUI;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ChatClient {
    public static void main(String args[]) throws IOException {
        Socket socket=new Socket("10.10.72.236",8888);
        System.out.println("请输入用户名");
        Scanner scanner=new Scanner(System.in);
        DataOutputStream dos=new DataOutputStream(socket.getOutputStream());
        String clientName=scanner.next();
        dos.writeUTF(clientName);
        new SendMessage(socket).start();
        new ReceiveMessage(socket).start();
    }
}
