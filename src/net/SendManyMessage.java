package net;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class SendManyMessage extends Thread{
    private String clientName;
    private Socket socket;
    public SendManyMessage(String clientName, Socket socket) {
        this.clientName=clientName;
        this.socket=socket;
    }
    public void run(){
        while (true){
            DataOutputStream dos= null;
            try {
                dos = new DataOutputStream(socket.getOutputStream());
                Scanner keyboard_dis=new Scanner(System.in);
                String message=keyboard_dis.next();
                dos.writeUTF(clientName+"  说： "+message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
