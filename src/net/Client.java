package net;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    Socket socket;  //客户端套接字
    public Client(String ip,int port){
        try {
            socket= new Socket(ip,port);   //向地址是ip，端口是port的服务器发送连接请求
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //发送信息
    public void sendMessage(String message){
        //PrintWriter是高端字符输出流,输出的目的地是sokect套接口的输出流，传到另外的远程电脑；
        try {
            PrintWriter printWriter=new PrintWriter(socket.getOutputStream());
            printWriter.println(message);
            printWriter.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static  void main(String args[]){
        Client client=new Client("10.10.72.236",8888);  //10.10.72.254,解析ip地址
        client.sendMessage("我是熊君丽");
    }
}
