package net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

//发消息
public class SendMessage extends  Thread{
    private Socket socket;

    public SendMessage(Socket socket){
        this.socket=socket;

    }
    public void run(){
        while (true){
            //键盘输入
            Scanner dis=new Scanner(System.in);
            try {
                //输出流，目的地是socket
                DataOutputStream dos=new DataOutputStream(socket.getOutputStream());
                //往输出流里写入键盘读到的字符串
                dos.writeUTF(dis.next());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
