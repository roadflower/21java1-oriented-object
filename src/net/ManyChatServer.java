package net;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class ManyChatServer {
    public static void main(String args[])  {
       //alt+enter
        try {
            //创建集合，用于保存客户端的昵称和socket
            Map<String,Socket>  clients=new HashMap<String, Socket>();
            ServerSocket serverSocket=new ServerSocket(9999);
            while(true){
                //接受客户端请求，建立socket
                Socket socket=serverSocket.accept();
                //创建sockect读入流，准备读取客户端输入的昵称
                DataInputStream dis=new DataInputStream(socket.getInputStream());
                //读取客户端输入的昵称
                String clientName=dis.readUTF();
                System.out.println(clientName+"  来了");
                //将客户端的昵称和socket保存在集合
                clients.put(clientName,socket);
                //启动广播线程，参数1：当前socket，参数2：广播面向群众的信息
                new BroadcastMessage(socket,clients).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
