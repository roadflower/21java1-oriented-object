package net;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ManyChatClient {
    public static void main(String args[]){
        try {
            //连接服务器，建立socket
            Socket socket=new Socket("10.10.72.236",9999);
            System.out.println("请输入用户名");
            //从键盘输入客户端昵称
            Scanner keyboard_dis=new Scanner(System.in);
            String clientName=keyboard_dis.next();
            //创建socket的输出流
            DataOutputStream dos=new DataOutputStream(socket.getOutputStream());
            //将昵称写入socket
            dos.writeUTF(clientName);
            //启动发消息和接收消息的线程
            new SendManyMessage(clientName,socket).start();
            new ReceiveMessage(socket).start();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
