package ch0200;

public class Person {
	private String nameString;
	private int age;
	private char gender;
	public Person() {
		
	}
   public Person(String nameString,int age,char gender) {
		this.nameString=nameString;
		this.age=age;
		this.gender=gender;
	}
	public String getNameString() {
		return nameString;
	}
	public void setNameString(String nameString) {
		this.nameString = nameString;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	
    public void print() {
    	System.out.println("my name is "+ nameString+",I am "+age+" yeas old, is "+ gender);
    }
}
