package ch0200;

import org.junit.Test;

public class Student extends Person {
	private String grade;
    public Student() {
    	this("21java1");
    }
    public Student(String grade) {
    	super("huang",21,'m');
    	this.grade=grade;
    	
    }

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	
	  //子类的方法与父类的同名，覆写
	  
	  public void print() { 
		  super.print();
		  System.out.println(",目前我上 "+grade); }
	 
	// 注解
	@Test
	public  void  main() {
		Teacher xiongTeacher = new Teacher();
		xiongTeacher.setNameString("xiong");
		xiongTeacher.setAge(45);
		xiongTeacher.setGender('f');
		xiongTeacher.setMajorField("java");
		xiongTeacher.print();
		
		Student huangStudent=new Student();
		huangStudent.setGrade("21");
		huangStudent.print();
		
	}
}
