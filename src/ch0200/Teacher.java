package ch0200;
public class Teacher extends Person  {
	private String majorField;

	public String getMajorField() {
		return majorField;
	}

	public void setMajorField(String majorField) {
		this.majorField = majorField;
	}
	public Teacher() {
		//this("java方向");
	}
	public Teacher(String name,int age,char gender,String majorField) {
		//调用父类构造方法
		super(name,age,gender);
		this.majorField=majorField;
	}

	  public void print() { 
		  //super,超级，父类
		  super.print();
		  System.out.println(",我的授课方向是 "+majorField); 
		  }
	 

}
