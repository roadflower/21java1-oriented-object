package ch0100;

public class Box {
	private float m_a;
	public Box() {
		
	}
	public Box(float parameter) {
		m_a=parameter;
	}
	public float getM_a() {
		return m_a;
	}
	public void setM_a(float m_a) {
		this.m_a = m_a;
	}
    public float getVolumn() {
		return m_a*m_a*m_a;
	}
    public float getArea() {
		return 6*m_a*m_a;
	}
    public void display(float result) {
		System.out.println(result);
	}
    public static float compute() {
    	return 6f;
    }

	public static void main(String[] args) {
		// TODO 自动生成的方法存根

        Box box=new Box();
		
		box.setM_a(5.6f);
		box.m_a=9.8f;
		box.display(box.getArea());
		box.display(box.getVolumn());
		
	}

}
