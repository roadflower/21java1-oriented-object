package ch0100;

public class HelloWorld {
	//属性，用户名和口令的数组，一堆User
	//方法，查找用户名对应的口令User,
    //方法，输入用户名的方法
	public static void main(String[] args) {
		/*
		 * // Student[] students=new Student[3]; // System.out.println(students); //
		 * System.out.println(students[0]); // System.out.println(students[1]); //
		 * System.out.println(students[2]); // Student zhangsan = new Student(); //
		 * zhangsan.setName("张三"); // zhangsan.setAge(10); // students[0]=zhangsan; //
		 * System.out.println(Math.max(5, 6)); // Student lisi = new Student(); //
		 * lisi.setClazz("21Java1"); // students[1]=lisi; // lisi=zhangsan; //
		 * System.out.println(students); // System.out.println(students[0]); //
		 * System.out.println(zhangsan); // System.out.println(students[1]); //
		 * System.out.println(lisi); // System.out.println(students[2]); //
		 * //System.out.println(zhangsan.getName() + ",age is " + zhangsan.getAge()); //
		 * // ctrl+1
		 */
	    //创造第一台手机，无参构造方法
		Phone  yoursPhone=new Phone();
		yoursPhone.setColorString("black");
		yoursPhone.setPrice(9000f);
		yoursPhone.printInfo();
		//创造第二台手机，有参构造方法
	    Phone p1=new Phone("6.5","white",6500,"xiaomi");
	    p1.printInfo();
	    //创造第三台手机
	    Phone myPhone=new Phone("7.5","pink");
	    myPhone.printInfo();
       
	  
	    
		/*
		 * Box box=new Box();
		 * 
		 * box.setM_a(5.6f);
		 * 
		 * box.display(box.getArea()); box.display(box.getVolumn());
		 */
	}

}
//source