package ch0100;
//类

public class Student {
	//属性,私有的
	private String studentId;  //学号
	private String name;      //名字
	private int age;  //年龄
	private String clazz; //班级

	//方法

	public  String haveLessons() {
		//终止+返回
		return "ok";
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
    //返回 学生的姓名，public公众的，公开的，对外暴露的
	public String getName() {
		return name;
	}
    //设置学生的姓名，赋值；入参
	public void setName(String name) {
		//this,Student类生成的当前对象
		this.name = name;
	}

	public int getAge() {
		Integer numberInteger=new Integer(10);
		return numberInteger;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	
	

}
