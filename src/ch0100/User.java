package ch0100;

public class User {
	//用户名
	private String nameString;
	//口令
	private  String pwdString;
	//构造函数
	public User() {

	}
	public User(String nameString, String pwdString) {
		
		this.nameString = nameString;
		this.pwdString = pwdString;
	}
		
	public String getNameString() {
		return nameString;
	}
	public void setNameString(String nameString) {
		this.nameString = nameString;
	}
	public String getPwdString() {
		return pwdString;
	}
	public void setPwdString(String pwdString) {
		this.pwdString = pwdString;
	}
	

}
