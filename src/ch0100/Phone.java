package ch0100;

public class Phone {
	private String sizeString;
	private String colorString;
	private float price;
	//品牌，华为，小米...,属性，成员变量
	private String typeString;
	//成员方法
	//无参构造方法
	public Phone() {
	}
	//有参构造方法1
	public Phone(String size,String color,float price,String type) {
		this.sizeString=size;
		this.colorString=color;
		this.price=price;
		this.typeString=type;
		
	}
	//有参构造方法2,(1)没有返回类型(2)方法名与类名相同，析构2
	public Phone(String size,String color) {
		this.sizeString=size;
		this.colorString=color;
		this.typeString="huawei";
	}
	
	public void printInfo() {
		System.out.println(sizeString+","+colorString+","+typeString);
	}

	public String getSizeString() {
		return sizeString;
	}

	public void setSizeString(String sizeString) {
		this.sizeString = sizeString;
	}

	public String getColorString() {
		return colorString;
	}

	public void setColorString(String colorString) {
		this.colorString = colorString;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getTypeString() {
		return typeString;
	}

	public void setTypeString(String typeString) {
		this.typeString = typeString;
	}
}
