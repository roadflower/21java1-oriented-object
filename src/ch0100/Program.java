package ch0100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Program {
	
	//获取键盘输入
	public static String input() {
		BufferedReader isr=new BufferedReader(new InputStreamReader(System.in));
		String nameString="";
		try {
			nameString = isr.readLine();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return nameString;
	}
	public static void main(String args[]) {
		Users users=new Users();
		String tagString="begin";
		while(!tagString.equals("end")) {
			tagString=input();
			System.out.println(users.searchPwd(tagString));
		}
	}
	

}
