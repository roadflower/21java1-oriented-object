package ch0100;

class Animal {
	 public void print() {
		 System.out.println("test");
	 }

}
//接口,抽象，没实现的,不要实现方法
interface Cat {
	// 写一个方法的定义
	public void printCat();
}
// 不能有多继承,用implements实施
class Dog extends Animal implements Cat {
//覆写父类的方法
	@Override
	public void printCat() {
		System.out.println("this is dog test");
	}

}

