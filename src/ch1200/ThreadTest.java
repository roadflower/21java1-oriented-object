package ch1200;

class MyThread extends Thread{
	 public MyThread(String name) {
		 super(name);
		// TODO 自动生成的构造函数存根
	}
	 public void run() {
		 for(int i=0;i<5;i++)
		 { System.out.println(this.getName()+" = "+i);
		 try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		 Thread.interrupted();
		 }
		 System.out.println(this.getName()+" done !");
	 }
}

public class ThreadTest {
	public static void main(String[] args) {
		// TODO 自动生成的方法存根
       Thread t1=new MyThread("Tom");
       Thread t2=new MyThread("Jerry");
       //线程的优先级1~10,10是最高优先级
       t1.setPriority(10);
       t2.setPriority(1);
       t1.start();
       t2.start();
	}

}
