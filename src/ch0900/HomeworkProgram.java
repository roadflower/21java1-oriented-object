package ch0900;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

class ClassName{
	public static String[] getClassNameData() {
		
		String[] classNames=new String[3];
		try {
		BufferedReader br=new BufferedReader(new FileReader("21java1\\className.txt"));
		
		String className;
		int i=0;
		while((className=br.readLine())!=null) {
			classNames[i]=className;
			i++;
		}
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return classNames;
	}
}
class StudentData{
	public static void getStudentData(String fileName) {
		try {
			BufferedReader br=new BufferedReader(new FileReader(fileName));
			ObjectOutputStream  oos=new ObjectOutputStream(new FileOutputStream("object\\"+fileName));
			String line;
			String[] studentInfo;
			Student  student;
			Map<String,Student> stuMap=new HashMap<>();
			while((line=br.readLine())!=null) {
				System.out.println(line);
				studentInfo=line.split("\t");
				student=new Student();
				student.setId(studentInfo[0]);
				student.setName(studentInfo[1]);
				int score=Integer.parseInt(studentInfo[2]);
				score=(score+60)>100?100:score+60;
				student.setScore(score);
				stuMap.put(studentInfo[0], student);
				oos.writeObject(student);

			}
			oos.writeObject(null);
			oos.close();
			br.close();
			System.out.println("查找到的学生信息是："+stuMap.get("0101120914"));
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public static void getStudentObjectData(String fileName) {
		try {
			
			ObjectInputStream  ois=new ObjectInputStream(new FileInputStream("object\\"+fileName));
			String line;
			String[] studentInfo;
			Student  student;
			while((student=(Student)ois.readObject())!=null) {

				System.out.println(student);
			}
			ois.close();
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
public class HomeworkProgram {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
          System.out.println("以下班级可供选择：");
          String[] classNames=ClassName.getClassNameData();
          for (int i = 0; i < classNames.length; i++) {
			System.out.println(classNames[i]);
		}
          InputStream is=System.in;
          System.out.println("请输入班级序号");
          try {
			int i=is.read();
			switch(i) {
			case '1':  StudentData.getStudentData("21java1\\Java1.txt");StudentData.getStudentObjectData("21java1\\Java1.txt");break;
			case '2':  StudentData.getStudentData("21java1\\\\Java2.txt");StudentData.getStudentObjectData("21java1\\Java2.txt");break;
			case '3':  StudentData.getStudentData("21java1\\\\Java3.txt");StudentData.getStudentObjectData("21java1\\Java3.txt");break;
			default:break;
			}
			
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

}
