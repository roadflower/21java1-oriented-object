package ch0900;

import java.io.Serializable;

public class Student implements Serializable{
	//private static final long serialVersionUID=1L;
	private String  id;
	private String name;
	private  int score;
	
	public Student() {
		super();
	}
	public Student(String id, String name, int score) {
		super();
		this.id = id;
		this.name = name;
		this.score = score;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String toString() {
		//“\t”表示tab键
		return id+"\t"+name+"\t"+score;
	}

}
