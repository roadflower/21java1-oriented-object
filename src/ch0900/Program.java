package ch0900;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedOutputStream;

public class Program {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		//标准的输入流--键盘System.in，标准的输出流--屏幕System.out
		//所有的输入输出流，异常
		FileOutputStream fos;
		try {
			/*
			 * //目的地是文件，字节，输出流 File file=new File("21java1"); //创建路径 file.mkdir();
			 */
			//fos当做是输出的管道，写数据,13是键盘“回车键”的ascii码
			/*
			 * fos=new FileOutputStream("21java1\\student.txt"); fos.write(97);
			 * fos.write(98); fos.write(32);
			 * 
			 * fos.write(48); fos.write(49); fos.write(13); fos.write(65); fos.write(66);
			 * //刷新缓存，将数据全部写入目的地 fos.flush(); //关闭输出流 fos.close();
			 
			FileInputStream fis=new FileInputStream("21java1\\student.txt");
			int i=0;
			do
			{
				i=fis.read();
				System.out.print((char)i);
			}while(i!=-1);
			fis.close();
			//文件字符输入流
			FileReader fReader=new FileReader("21java1\\student.txt");
			//文件字符输出流
			FileWriter fWriter=new FileWriter("21java1\\newStudent.txt");
			int i=0;
			do {
				i=fReader.read();
				System.out.print((char)i);
				//写
				fWriter.write(i);
				
			}while(i!=-1);
			fReader.close();
			fWriter.flush();fWriter.close();*/
           BufferedReader bufferedReader=new BufferedReader(new FileReader("21java1\\student.txt"));
           BufferedWriter bufferedWriter=new BufferedWriter(new FileWriter("21java1\\newStudent1.txt"));
          String line="";
          line=bufferedReader.readLine();
          do{
   	 
        	  System.out.println(line);
        	  bufferedWriter.write(line);
        	  bufferedWriter.newLine();
        	  line=bufferedReader.readLine();
          }while(line!=null) ;
          bufferedReader.close();
			bufferedWriter.flush();
			bufferedWriter.close();
			
		} catch (FileNotFoundException  e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

		
	}

}
