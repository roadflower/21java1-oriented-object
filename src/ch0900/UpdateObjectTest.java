package ch0900;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.imageio.spi.IIOServiceProvider;

public class UpdateObjectTest {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("0930\\updateStudent"));

			Student s = null;
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("0930\\updateStudent"));
			  while((s=(Student)ois.readObject())!=null) { 
				  //修改读出来的对象的成绩 
				  s.setScore(100);
				  System.out.println(s);
			  	  //将修改后的对象s写回到新文件 updateStudent 
				  oos.writeObject(s); 
				  }
			 oos.writeObject(null);

			ois.close();
			 oos.flush();
			 oos.close();

		}  catch (FileNotFoundException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

}
