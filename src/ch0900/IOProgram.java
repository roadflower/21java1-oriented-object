package ch0900;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class IOProgram {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		try {
			//文件字节读入流
			FileInputStream  fis=new FileInputStream("21java1\\student.txt");
			//文件字符读入流
			FileOutputStream  fos=new FileOutputStream("21java1\\bytecode.txt");
			FileReader fr=new FileReader("21java1\\\\student.txt");
			FileWriter fw=new FileWriter("21java1\\charwriter.txt");
			//用c存储从流读到的字节
			int c;
			while((c=fis.read())!=-1){
			   System.out.print((char)c);
			   //字节输出流，不使用缓存，每个字节老老实实写到目的地了
			   fos.write(c);
			}
			System.out.println("\n\r");
			while((c=fr.read())!=-1) {
				System.out.print((char)c);
				//字符流，使用了缓存，目的地没有内容
				fw.write(c);
			}
			//从缓存冲刷到目的地
			fw.flush();
			fw.close();
			
		} catch (FileNotFoundException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
		

	}

}
