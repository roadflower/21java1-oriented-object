package ch1000;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ch0900.Student;

public class MapProgram {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
         Map<String,Integer> myMap=new LinkedHashMap<String,Integer>();
         Student student1=new Student("01","apolo",80);
         Student student2=new Student("02","joysi",90);
         //存放 的   键key 值value  对
         myMap.put("a", 90);
         myMap.put("b", 100);
         myMap.put("c", 50);
         myMap.put("d", 34);
         myMap.put("e", 99);
       
         //通过 键 查找 值
        // System.out.println(myMap.get("first"));
       System.out.println(myMap);
      
         
	}

}
class MyCompare1 implements Comparator<Map.Entry<String, Integer>>{
	@Override
	public int compare(Map.Entry<String, Integer>  o1, Map.Entry<String, Integer> o2) {
		// TODO 自动生成的方法存根
		return o1.getValue()-o2.getValue();
	}
}
