package ch1000;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class LinkedListProgram {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
        //链列表，非连续内存空间
		LinkedList list=new LinkedList<>();
		try {
			BufferedReader bufferedReader=new BufferedReader(new FileReader("21java1\\className.txt"));
			String line;
			while((line=bufferedReader.readLine())!=null) {
				list.add(line);
			}
			Iterator iterator=list.iterator();
			while(iterator.hasNext())
				System.out.println(iterator.next());
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
	
	}

}
